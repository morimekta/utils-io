Utilities for I/O
=================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils-io&color=informational)](https://morimekta.net/utils-io/)
[![Docs](https://www.javadoc.io/badge/net.morimekta.utils/io.svg)](https://www.javadoc.io/doc/net.morimekta.utils/io)
[![Pipeline](https://gitlab.com/morimekta/utils-io/badges/master/pipeline.svg)](https://gitlab.com/morimekta/utils-io/pipelines)
[![Coverage](https://gitlab.com/morimekta/utils-io/badges/master/coverage.svg)](https://morimekta.net/utils-io/jacoco-ut/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)  
Java module with utilities for handling binary IO and controlling IO mode on a *NIX terminal.
See [morimekta.net/utils](https://morimekta.net/utils/) for procedures on releases.

## Getting Started

To add to `maven`: Add this line to `pom.xml` under dependencies:

```xml

<dependency>
    <groupId>net.morimekta.utils</groupId>
    <artifactId>io</artifactId>
    <version>${version}</version>
</dependency>
```

To add to `gradle`: Add this line to the `dependencies` group in `build.gradle`:

```
implementation 'net.morimekta.utils:io:${version}'
```

## I/O Streams

The input and output streams comes in pairs, and have various specified jobs.

- `BigEndianBinary` | `LittleEndianBinary` IO: Handling reading binary content from a wrapped stream handling numbers as
  big endian or little endian. Also handles zigzag encoding of numbers, BASE-128 etc.
- `ByteBuffer` IO: Wrapping byte buffers for reading and writing using IO streams.
- `BitPacking` IO: Will write individual bits to stream, and continue after the last bit.
- `SeptetPacking` IO: Limited to write "septets" (7-bit chunks), packing each byte in 7 bits each (ignoring overflow).
- `ForkingOutpusStream`: Forks written bytes to multiple output streams. Any write will be attempted to each stream,
  then decided if exceptions are thrown (if any failed).
- `NoCloseOutputStream`: An output stream wrapper that does not close its contained output stream.

### Sub I/O Streams

- `Framed`: The frames streams are streams of a pre-given size, usually by bytes of the start of the stream. They will
  act as a stream of that size when reading, and will buffer up bytes and write to wrapped stream including the size
  when closed when writing.
- `Terminated`: Instead of having a fixed size. Will terminate the stream when encountering a specific byte. When
  writing will write the terminating byte to stream when closed.

## Terminal Handling

The `tty` classes are controlling terminal (tty) IO modes, and getting the current terminal size.

## SubProcess

The `proc` package contains a utility for easy running of a subprocess with control over input to the subprocess, and
getting output from it. It turns the streams attached to the native `Process` object, and let the user connect streams
to the process to be updated live, which will be properly closed when the process stops or reaches deadline.
