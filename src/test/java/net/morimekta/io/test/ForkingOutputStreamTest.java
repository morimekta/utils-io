package net.morimekta.io.test;

import net.morimekta.io.ForkingOutputStream;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ForkingOutputStreamTest {
    @Test
    public void testConstructor() {
        try {
            new ForkingOutputStream((OutputStream[]) null);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No output"));
        }
        try {
            new ForkingOutputStream((Collection<OutputStream>) null);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No output"));
        }
        try {
            new ForkingOutputStream();
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No output"));
        }
        try {
            new ForkingOutputStream(List.of());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No output"));
        }
    }

    @Test
    public void testWrite_byte() throws IOException {
        OutputStream a = mock(OutputStream.class);
        OutputStream b = mock(OutputStream.class);
        OutputStream c = mock(OutputStream.class);

        ForkingOutputStream fos = new ForkingOutputStream(a, b, c);
        fos.write(5);

        verify(a).write(5);
        verify(b).write(5);
        verify(c).write(5);
        verifyNoMoreInteractions(a, b, c);

        reset(a, b);
        doThrow(new IOException("foo")).when(a).write(42);
        doThrow(new IOException("bar")).when(c).write(42);

        try {
            fos.write(42);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
            assertThat(e.getSuppressed(), is(notNullValue()));
            assertThat(e.getSuppressed().length, is(1));
            assertThat(e.getSuppressed()[0].getMessage(), is("bar"));
        }

        verify(a).write(42);
        verify(b).write(42);
        verify(c).write(42);
        verifyNoMoreInteractions(a, b, c);
    }

    @Test
    public void testWrite_bytes() throws IOException {
        OutputStream a = mock(OutputStream.class);
        OutputStream b = mock(OutputStream.class);
        OutputStream c = mock(OutputStream.class);

        ForkingOutputStream fos = new ForkingOutputStream(List.of(a, b, c));

        fos.write(new byte[]{5});

        verify(a).write(new byte[]{5}, 0, 1);
        verify(b).write(new byte[]{5}, 0, 1);
        verify(c).write(new byte[]{5}, 0, 1);
        verifyNoMoreInteractions(a, b, c);

        reset(a, b);
        doThrow(new IOException("foo")).when(a).write(new byte[]{4, 2}, 0, 2);
        doThrow(new IOException("bar")).when(c).write(new byte[]{4, 2}, 0, 2);

        try {
            fos.write(new byte[]{4, 2});
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
            assertThat(e.getSuppressed(), is(notNullValue()));
            assertThat(e.getSuppressed().length, is(1));
            assertThat(e.getSuppressed()[0].getMessage(), is("bar"));
        }

        verify(a).write(new byte[]{4, 2}, 0, 2);
        verify(b).write(new byte[]{4, 2}, 0, 2);
        verify(c).write(new byte[]{4, 2}, 0, 2);
        verifyNoMoreInteractions(a, b, c);
    }

    @Test
    public void testWrite_bytesOffset() throws IOException {
        OutputStream a = mock(OutputStream.class);
        OutputStream b = mock(OutputStream.class);
        OutputStream c = mock(OutputStream.class);

        ForkingOutputStream fos = new ForkingOutputStream(List.of(a, b, c));

        fos.write(new byte[]{5}, 0, 1);

        verify(a).write(new byte[]{5}, 0, 1);
        verify(b).write(new byte[]{5}, 0, 1);
        verify(c).write(new byte[]{5}, 0, 1);
        verifyNoMoreInteractions(a, b, c);

        reset(a, b);
        doThrow(new IOException("foo")).when(a).write(new byte[]{4, 2}, 0, 2);
        doThrow(new IOException("bar")).when(c).write(new byte[]{4, 2}, 0, 2);

        try {
            fos.write(new byte[]{4, 2}, 0, 2);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
            assertThat(e.getSuppressed(), is(notNullValue()));
            assertThat(e.getSuppressed().length, is(1));
            assertThat(e.getSuppressed()[0].getMessage(), is("bar"));
        }

        verify(a).write(new byte[]{4, 2}, 0, 2);
        verify(b).write(new byte[]{4, 2}, 0, 2);
        verify(c).write(new byte[]{4, 2}, 0, 2);
        verifyNoMoreInteractions(a, b, c);
    }

    @Test
    public void testClose() throws IOException {
        OutputStream a = mock(OutputStream.class);
        OutputStream b = mock(OutputStream.class);
        OutputStream c = mock(OutputStream.class);

        ForkingOutputStream fos = new ForkingOutputStream(List.of(a, b, c));
        fos.close();

        verify(a).close();
        verify(b).close();
        verify(c).close();
        verifyNoMoreInteractions(a, b, c);

        reset(a, b, c);

        fos.close();
        verifyNoInteractions(a, b, c);

        reset(a, b, c);

        doThrow(new IOException("foo")).when(a).close();
        doThrow(new IOException("bar")).when(c).close();

        fos = new ForkingOutputStream(List.of(a, b, c));
        try {
            fos.close();
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
            assertThat(e.getSuppressed(), is(notNullValue()));
            assertThat(e.getSuppressed().length, is(1));
            assertThat(e.getSuppressed()[0].getMessage(), is("bar"));
        }

        verify(a).close();
        verify(b).close();
        verify(c).close();
        verifyNoMoreInteractions(a, b, c);

        reset(a, b, c);

        fos.close();
        verifyNoInteractions(a, b, c);
    }


    @Test
    public void testFlush() throws IOException {
        OutputStream a = mock(OutputStream.class);
        OutputStream b = mock(OutputStream.class);
        OutputStream c = mock(OutputStream.class);

        ForkingOutputStream fos = new ForkingOutputStream(List.of(a, b, c));
        fos.flush();

        verify(a).flush();
        verify(b).flush();
        verify(c).flush();
        verifyNoMoreInteractions(a, b, c);

        reset(a, b, c);

        doThrow(new IOException("foo")).when(a).flush();
        doThrow(new IOException("bar")).when(c).flush();

        fos = new ForkingOutputStream(List.of(a, b, c));
        try {
            fos.flush();
        } catch (IOException e) {
            assertThat(e.getMessage(), is("foo"));
            assertThat(e.getSuppressed(), is(notNullValue()));
            assertThat(e.getSuppressed().length, is(1));
            assertThat(e.getSuppressed()[0].getMessage(), is("bar"));
        }

        verify(a).flush();
        verify(b).flush();
        verify(c).flush();
        verifyNoMoreInteractions(a, b, c);
    }
}
