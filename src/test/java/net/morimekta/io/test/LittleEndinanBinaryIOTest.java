/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.test;

import net.morimekta.io.BigEndianBinaryInputStream;
import net.morimekta.io.BinaryInputStream;
import net.morimekta.io.BinaryOutputStream;
import net.morimekta.io.LittleEndianBinaryInputStream;
import net.morimekta.io.LittleEndianBinaryOutputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Test testing the pairing between BinaryOutputStream and BinaryInputStream, and that
 * what it writes will be read back exactly the same.
 */
public class LittleEndinanBinaryIOTest extends BinaryIOTestBase {
    @Override
    protected BinaryOutputStream makeOutputStream(OutputStream out) {
        return new LittleEndianBinaryOutputStream(out);
    }

    @Override
    protected BinaryInputStream makeInputStream(InputStream in) {
        return new LittleEndianBinaryInputStream(in);
    }

    @Test
    public void testReadEndianNess() throws IOException {
        out.write(new byte[]{4, 0});
        assertEquals((short) 4, getReader().expectShort());

        out.write(new byte[]{4, 0, 0, 0});
        assertEquals(4, getReader().expectInt());

        out.write(new byte[]{4, 0, 0, 0, 0, 0, 0, 0});
        assertEquals((short) 4, getReader().expectLong());

        out.write(new byte[]{4, 0});
        assertEquals((short) 4, getReader().expectUInt16());

        out.write(new byte[]{4, 0, 0});
        assertEquals((short) 4, getReader().expectUInt24());

        out.write(new byte[]{4, 0, 0, 0});
        assertEquals((short) 4, getReader().expectUInt32());
    }

    @Test
    public void testWriteEndianNess() throws IOException {
        writer.writeShort((short) 4);
        assertArrayEquals(new byte[]{4, 0},
                          out.toByteArray());
        out.reset();

        writer.writeInt(4);
        assertArrayEquals(new byte[]{4, 0, 0, 0},
                          out.toByteArray());
        out.reset();

        writer.writeLong(4L);
        assertArrayEquals(new byte[]{4, 0, 0, 0, 0, 0, 0, 0},
                          out.toByteArray());
        out.reset();

        writer.writeUInt16(4);
        assertArrayEquals(new byte[]{4, 0},
                          out.toByteArray());
        out.reset();

        writer.writeUInt24(4);
        assertArrayEquals(new byte[]{4, 0, 0},
                          out.toByteArray());
        out.reset();

        writer.writeUInt32(4);
        assertArrayEquals(new byte[]{4, 0, 0, 0},
                          out.toByteArray());
        out.reset();
    }

    @Test
    public void testRead() throws IOException {
        out.write(new byte[]{1, 2, 3, 4});

        BinaryInputStream reader = getReader();

        assertEquals(1, reader.read());

        byte[] tmp = new byte[10];
        assertEquals(3, reader.read(tmp, 2, 8));
        assertArrayEquals(new byte[]{0, 0, 2, 3, 4, 0, 0, 0, 0, 0},
                          tmp);

        try {
            reader.read(tmp, 5, 6);
        } catch (IllegalArgumentException e) {
            assertEquals("Illegal arguments for read: byte[10], off:5, len:6",
                         e.getMessage());
        }
    }

    @Test
    public void tesExpectTooSmall() {
        byte[] arr = new byte[12];
        new Random().nextBytes(arr);
        ByteArrayInputStream in = new ByteArrayInputStream(arr);
        BigEndianBinaryInputStream reader = new BigEndianBinaryInputStream(in);

        try {
            reader.expectBytes(arr.length + 2);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Not enough data available on stream: 12 < 14"));
        }
    }

    @Test
    public void testWrite() throws IOException {
        writer.write(new byte[]{1, 2, 3, 4}, 1, 2);
        writer.write(0x7f);

        byte[] tmp = new byte[4];
        assertEquals(3, getReader().read(tmp));
        assertArrayEquals(new byte[]{2, 3, 0x7f, 0}, tmp);
    }
}
