package net.morimekta.io.test.proc;

import net.morimekta.io.proc.SubProcessRunner;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.Executors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SubProcessRunnerTest {
    @TempDir
    public File tempDir;

    @Test
    public void testBadSetters() {
        SubProcessRunner runner = new SubProcessRunner();
        try {
            runner.setRuntime(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("runtime == null"));
        }
        try {
            runner.setThreadFactory(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("threadFactory == null"));
        }
        try {
            runner.setWorkingDir(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("workingDir == null"));
        }
        try {
            runner.setOut(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("out == null"));
        }
        try {
            runner.setErr(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("err == null"));
        }
        try {
            runner.setDeadlineMs(-1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("deadlineMs: -1 < 0"));
        }
        try {
            runner.setDeadlineFlushMs(-1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("deadlineFlushMs: -1 < 0"));
        }
    }

    @Test
    public void testSetters() throws IOException {
        SubProcessRunner runner = new SubProcessRunner();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        runner.setOut(out);
        runner.setErr(err);

        runner.setThreadFactory(Executors.defaultThreadFactory());
        runner.setWorkingDir(tempDir.toPath());
        runner.setRuntime(Runtime.getRuntime());

        assertThat(runner.exec("ls"), is(0));

        assertThat(err.toString(), is(""));
        assertThat(out.toString(), is(""));
    }

    @Test
    public void testExec_NoParams() throws IOException {
        SubProcessRunner runner = new SubProcessRunner();
        assertThat(runner.exec("ls", "-l"), is(0));
    }

    @Test
    public void testExec_SimpleOutput() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        SubProcessRunner runner = new SubProcessRunner();
        runner.setOut(out);
        runner.setErr(err);

        assertThat(runner.exec("echo", "foo"), is(0));
        assertThat(err.toString(), is(""));
        assertThat(out.toString(), is("foo\n"));
    }

    @Test
    public void testExec_SimpleError() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        SubProcessRunner runner = new SubProcessRunner();
        runner.setOut(out);
        runner.setErr(err);

        assertThat(runner.exec("ls", "--foo_bar"), is(not(0)));
        assertThat(out.toString(), is(""));
        assertThat(err.toString(), containsString("--foo_bar"));
    }

    @Test
    public void testExec_SimpleInput() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream("foo\n".getBytes(UTF_8));
        SubProcessRunner runner = new SubProcessRunner();
        runner.setOut(out);
        runner.setErr(err);

        assertThat(runner.exec(in, "cat"), is(0));
        assertThat(err.toString(), is(""));
        assertThat(out.toString(), is("foo\n"));
    }

    @Test
    public void testExec_DeadlineOK() throws IOException {
        SubProcessRunner runner = new SubProcessRunner();
        runner.setDeadlineMs(200);
        runner.setDeadlineFlushMs(100);

        assertThat(runner.exec("ls"), is(0));
    }

    @Test
    public void testExec_DeadlineExceeded() {
        SubProcessRunner runner = new SubProcessRunner();
        runner.setDeadlineMs(200);
        runner.setDeadlineFlushMs(100);

        try {
            runner.exec("sh", "-c", "sleep 2");
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), allOf(
                    startsWith("deadline exceeded: "),
                    endsWith(" > 200: sh -c \"sleep 2\"")));
        }
    }

    @Test
    public void testExec_WithEnv() throws IOException {
        SubProcessRunner runner = new SubProcessRunner();
        runner.addToEnv(Map.of("FOO", "BAR"));
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        runner.setOut(out);
        assertThat(runner.exec("sh", "-c", "echo $FOO"), is(0));
        assertThat(out.toString(UTF_8), is("BAR\n"));
    }

    @Test
    public void testExec_OutputException() throws IOException, InterruptedException {
        SubProcessRunner runner = new SubProcessRunner();

        InputStream stdOut = mock(InputStream.class);
        when(stdOut.read(any(byte[].class))).thenThrow(new IOException("read"));
        doThrow(new IOException("close")).when(stdOut).close();

        Process process = mock(Process.class);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("foo".getBytes(UTF_8)));
        when(process.getOutputStream()).thenReturn(new ByteArrayOutputStream());
        when(process.getInputStream()).thenReturn(stdOut);

        Runtime runtime = mock(Runtime.class);
        when(runtime.exec(any(String[].class), any(String[].class))).thenReturn(process);
        runner.setRuntime(runtime);

        try {
            runner.exec("foo");
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("read"));
            assertThat(e.getSuppressed().length, is(0));
        }
    }

    @Test
    public void testExec_InputException() throws IOException, InterruptedException {
        SubProcessRunner runner = new SubProcessRunner();

        OutputStream in = mock(OutputStream.class);
        doThrow(new IOException("write")).when(in).write(any(byte[].class), anyInt(), anyInt());
        doThrow(new IOException("close")).when(in).close();

        Process process = mock(Process.class);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("foo".getBytes(UTF_8)));
        when(process.getInputStream()).thenReturn(new ByteArrayInputStream("foo".getBytes(UTF_8)));
        when(process.getOutputStream()).thenReturn(in);

        Runtime runtime = mock(Runtime.class);
        when(runtime.exec(any(String[].class), any(String[].class))).thenReturn(process);
        runner.setRuntime(runtime);

        try {
            runner.exec(new ByteArrayInputStream("foo".getBytes(UTF_8)), "foo");
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("write"));
            assertThat(e.getSuppressed().length, is(0));
        }
    }

    @Test
    public void testExec_Interrupted() throws InterruptedException, IOException {
        SubProcessRunner runner = new SubProcessRunner();

        Process process = mock(Process.class);
        when(process.getOutputStream()).thenReturn(OutputStream.nullOutputStream());
        when(process.getErrorStream()).thenReturn(InputStream.nullInputStream());
        when(process.getInputStream()).thenReturn(InputStream.nullInputStream());
        when(process.waitFor()).thenThrow(new InterruptedException("interrupted"));

        Runtime runtime = mock(Runtime.class);
        when(runtime.exec(any(String[].class), any(String[].class))).thenReturn(process);
        runner.setRuntime(runtime);

        try {
            runner.exec(new ByteArrayInputStream("foo".getBytes(UTF_8)), "foo");
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("interrupted"));
            assertThat(e.getSuppressed().length, is(0));
        }
    }
}
