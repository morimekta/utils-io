package net.morimekta.io.test.proc;

import net.morimekta.io.proc.SubProcess;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SubProcessTest {
    @TempDir
    public File tempDir;

    @Test
    public void testSimple() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream("foo\n".getBytes(UTF_8));
        SubProcess sp = SubProcess.newRunner("cat")
                                  .withOut(out)
                                  .withErr(err)
                                  .withIn(in)
                                  .withDeadlineMs(1000)
                                  .withDeadlineFlushMs(100)
                                  .withRuntime(Runtime.getRuntime())
                                  .withWorkingDir(tempDir.toPath())
                                  .withThreadFactory(Executors.defaultThreadFactory())
                                  .run();
        assertThat(sp.getExitCode(), is(0));
        assertThat(sp.getOutput(), is("foo\n"));
        assertThat(sp.getError(), is(""));

        assertThat(out.toString(), is("foo\n"));
        assertThat(err.toString(), is(""));
    }

    @Test
    public void testWithEnv() throws IOException {
        SubProcess sp = SubProcess.newRunner("sh", "-c", "echo $FOO")
                                  .withEnv("FOO", "BAR")
                                  .run();
        assertThat(sp.getOutput(), is("BAR\n"));
    }
}
