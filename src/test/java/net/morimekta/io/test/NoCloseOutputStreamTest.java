package net.morimekta.io.test;

import net.morimekta.io.NoCloseOutputStream;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class NoCloseOutputStreamTest {
    @Test
    public void testDelegate() throws IOException {
        var mock = mock(OutputStream.class);
        try (NoCloseOutputStream out = new NoCloseOutputStream(mock)) {
            out.write(1);
            out.write(new byte[]{1, 2, 3});
            out.write(new byte[]{4, 5, 6}, 1, 1);
            out.flush();
        }
        verify(mock).write(1);
        verify(mock).write(new byte[]{1, 2, 3});
        verify(mock).write(new byte[]{4, 5, 6}, 1, 1);
        verify(mock).flush();
        verifyNoMoreInteractions(mock);
    }

    @Test
    public void testClosed() throws IOException {
        var mock = mock(OutputStream.class);
        NoCloseOutputStream out = new NoCloseOutputStream(mock);
        try (out) {
            out.write(1);
        }
        verify(mock).write(1);

        assertThrows(IOException.class, () -> {
            out.write(1);
        });
        assertThrows(IOException.class, () -> {
            out.write(new byte[]{1, 2, 3});
        });
        assertThrows(IOException.class, () -> {
            out.write(new byte[]{4, 5, 6}, 1, 1);
        });
        // No exception on flush.
        out.flush();
        // No exception on close.
        out.close();

        verifyNoMoreInteractions(mock);
    }
}
