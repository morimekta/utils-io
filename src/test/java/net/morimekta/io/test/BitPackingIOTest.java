package net.morimekta.io.test;

import net.morimekta.io.BitPackingInputStream;
import net.morimekta.io.BitPackingOutputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class BitPackingIOTest {
    @Test
    public void testSimpleBits() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BitPackingOutputStream bout = new BitPackingOutputStream(out);

        bout.writeBits(1, 1);
        bout.writeBits(2, 2);
        bout.writeBits(3, 3);
        assertThat(bout.getWrittenBits(), is(6));
        bout.writeBits(5, 5);
        bout.write(8);
        assertThat(bout.getWrittenBits(), is(19));
        bout.align();
        bout.writeBits(13, 13);
        bout.writeBits(21, 21);
        assertThat(bout.getWrittenBits(), is(53));
        bout.flush();
        bout.close();

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        BitPackingInputStream bin = new BitPackingInputStream(in);

        assertThat(bin.readBits(1), is(1));
        assertThat(bin.readBits(2), is(2));
        assertThat(bin.readBits(3), is(3));
        assertThat(bin.getReadBits(), is(6));
        assertThat(bin.available(), is(7));
        assertThat(bin.readBits(5), is(5));
        assertThat(bin.read(), is(8));
        assertThat(bin.getReadBits(), is(19));
        assertThat(bin.available(), is(5));
        bin.align();
        assertThat(bin.readBits(13), is(13));
        assertThat(bin.readBits(21), is(21));
        assertThat(bin.getReadBits(), is(53));
        assertThat(bin.available(), is(0));
        assertThat(bin.readBits(31), is(-1));
        assertThat(bin.available(), is(0));

        bin.close();

        assertThat(bin.available(), is(0));
    }

    @Test
    public void testClosedStream() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BitPackingOutputStream bout = new BitPackingOutputStream(out);

        bout.close();

        try {
            bout.write(44);
            fail("No exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Writing to closed stream"));
        }

        bout.close();

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        BitPackingInputStream bin = new BitPackingInputStream(in);

        bin.close();

        try {
            bin.read();
            fail("No exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Reading from closed stream"));
        }

        bin.close();
    }

    @Test
    public void testBadParams() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BitPackingOutputStream bout = new BitPackingOutputStream(out);

        try {
            bout.writeBits(0, 55);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Illegal writing bit count 0"));
        }
        try {
            bout.writeBits(32, 55);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Illegal writing bit count 32"));
        }
        try {
            bout.writeBits(5, -55);
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Writing negative bit data: -55"));
        }

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        BitPackingInputStream bin = new BitPackingInputStream(in);

        try {
            fail("No exception: " + bin.readBits(0));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Trying to read 0 bits"));
        }
        try {
            fail("No exception: " + bin.readBits(32));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Trying to read 32 bits, more than max 31 allowed"));
        }
        try {
            fail("No exception: " + bin.available(0));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Trying to read 0 bits"));
        }
        try {
            fail("No exception: " + bin.available(32));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Trying to read 32 bits, more than max 31 allowed"));
        }

    }
}
