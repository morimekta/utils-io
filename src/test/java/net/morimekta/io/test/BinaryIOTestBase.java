/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.test;

import net.morimekta.io.BinaryInputStream;
import net.morimekta.io.BinaryOutputStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Random;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Test testing the pairing between BinaryWriter and BinaryReader, and that
 * what it writes will be read back exactly the same.
 */
public abstract class BinaryIOTestBase {
    protected ByteArrayOutputStream out;
    protected BinaryOutputStream    writer;

    protected abstract BinaryOutputStream makeOutputStream(OutputStream out);

    protected abstract BinaryInputStream makeInputStream(InputStream in);

    @BeforeEach
    public void setUp() {
        out = new ByteArrayOutputStream();
        writer = makeOutputStream(out);
    }

    protected BinaryInputStream getReader() {
        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
        out.reset();
        return makeInputStream(in);
    }

    @Test
    public void testClose() throws IOException {
        OutputStream out = mock(OutputStream.class);
        InputStream in = mock(InputStream.class);

        makeOutputStream(out).close();
        makeInputStream(in).close();

        verify(in).close();
        verify(out).close();
        verifyNoMoreInteractions(out, in);
    }

    @Test
    public void testBytesRoundTrip() throws IOException {
        byte[] bytes = new byte[]{0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        // test writing and reading bytes.
        writer.write(bytes);

        BinaryInputStream reader = getReader();

        byte[] read = new byte[bytes.length];
        assertThat(reader.read(read), is(bytes.length));

        assertArrayEquals(bytes, read);
    }

    @Test
    public void testByte() throws IOException {
        // test writing and reading bytes.
        writer.writeByte((byte) 1);
        writer.writeByte((byte) 0xff);
        writer.writeByte((byte) '\"');
        writer.writeByte((byte) 0);

        BinaryInputStream reader = getReader();

        assertEquals((byte) 1, reader.expectByte());
        assertEquals((byte) 0xff, reader.expectByte());
        assertEquals((byte) '\"', reader.expectByte());
        assertEquals((byte) 0, reader.expectByte());

        try {
            reader.expectByte();
        } catch (IOException e) {
            assertEquals("Missing expected byte", e.getMessage());
        }
    }

    @Test
    public void testShort() throws IOException {
        // test writing and reading shorts.
        writer.writeShort((short) 1);
        writer.writeShort((short) 0xffff);
        writer.writeShort((short) -12345);
        writer.writeShort((short) 0);

        BinaryInputStream reader = getReader();

        assertEquals((short) 1, reader.expectShort());
        assertEquals((short) 0xffff, reader.expectShort());
        assertEquals((short) -12345, reader.expectShort());
        assertEquals((short) 0, reader.expectShort());
    }

    @Test
    public void testBadShort() {
        assertBadExpectShort("Missing byte 1 to expected short", new byte[]{});
        assertBadExpectShort("Missing byte 2 to expected short", new byte[]{0});
    }

    private void assertBadExpectShort(String message, byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try {
            BinaryInputStream reader = makeInputStream(bais);
            reader.expectShort();
            fail("No exception on bad short");
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }

    @Test
    public void testInt() throws IOException {
        // test writing and reading shorts.
        writer.writeInt(1);
        writer.writeInt(0xdeadbeef);
        writer.writeInt(0xffffffff);
        writer.writeInt(-1234567890);
        writer.writeInt(0);

        BinaryInputStream reader = getReader();

        assertEquals(1, reader.expectInt());
        assertEquals(0xdeadbeef, reader.expectInt());
        assertEquals(0xffffffff, reader.expectInt());
        assertEquals(-1234567890, reader.expectInt());
        assertEquals(0, reader.expectInt());
    }

    @Test
    public void testBadInt() {
        assertBadExpectInt("Missing byte 1 to expected int", new byte[]{});
        assertBadExpectInt("Missing byte 2 to expected int", new byte[]{0});
        assertBadExpectInt("Missing byte 3 to expected int", new byte[]{0, 0});
        assertBadExpectInt("Missing byte 4 to expected int", new byte[]{0, 0, 0});
    }

    private void assertBadExpectInt(String message, byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try {
            BinaryInputStream reader = makeInputStream(bais);
            reader.expectInt();
            fail("No exception on bad int");
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }

    @Test
    public void testLong() throws IOException {
        // test writing and reading shorts.
        writer.writeLong(1);
        writer.writeLong(0xdeadbeefcafebabeL);
        writer.writeLong(0xffffffffffffffffL);
        writer.writeLong(-1234567890123456789L);
        writer.writeLong(0);

        BinaryInputStream reader = getReader();

        assertEquals(1, reader.expectLong());
        assertEquals(0xdeadbeefcafebabeL, reader.expectLong());
        assertEquals(0xffffffffffffffffL, reader.expectLong());
        assertEquals(-1234567890123456789L, reader.expectLong());
        assertEquals(0, reader.expectLong());
    }

    @Test
    public void testBadLong() {
        assertBadExpectLong("Missing byte 1 to expected long", new byte[]{});
        assertBadExpectLong("Missing byte 2 to expected long", new byte[]{0});
        assertBadExpectLong("Missing byte 3 to expected long", new byte[]{0, 0});
        assertBadExpectLong("Missing byte 4 to expected long", new byte[]{0, 0, 0});
        assertBadExpectLong("Missing byte 5 to expected long", new byte[]{0, 0, 0, 0});
        assertBadExpectLong("Missing byte 6 to expected long", new byte[]{0, 0, 0, 0, 0});
        assertBadExpectLong("Missing byte 7 to expected long", new byte[]{0, 0, 0, 0, 0, 0});
        assertBadExpectLong("Missing byte 8 to expected long", new byte[]{0, 0, 0, 0, 0, 0, 0});
    }

    private void assertBadExpectLong(String message, byte[] data) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try {
            BinaryInputStream reader = makeInputStream(bais);
            reader.expectLong();
            fail("No exception on bad long");
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }

    @Test
    public void testDouble() throws IOException {
        // test writing and reading shorts.
        writer.writeDouble(1);
        writer.writeDouble(6.62607004E-34);
        writer.writeDouble(299792458);
        writer.writeDouble(-123456.123456);
        writer.writeDouble(0.0);

        BinaryInputStream reader = getReader();

        assertEquals(1.0, reader.expectDouble(), 0.0);
        assertEquals(6.62607004E-34, reader.expectDouble(), 0.0);
        assertEquals(299792458, reader.expectDouble(), 0.0);
        assertEquals(-123456.123456, reader.expectDouble(), 0.0);
        assertEquals(0.0, reader.expectDouble(), 0.0);
    }

    @Test
    public void testFloat() throws IOException {
        // test writing and reading shorts.
        writer.writeFloat(1);
        writer.writeFloat(6.62607004E-34f);
        writer.writeFloat(299792458);
        writer.writeFloat(-123456.123456f);
        writer.writeFloat(0.0f);

        BinaryInputStream reader = getReader();

        assertEquals(1.0f, reader.expectFloat(), 0.0f);
        assertEquals(6.62607004E-34f, reader.expectFloat(), 0.0f);
        assertEquals(299792458f, reader.expectFloat(), 0.0f);
        assertEquals(-123456.123456f, reader.expectFloat(), 0.0f);
        assertEquals(0.0f, reader.expectFloat(), 0.0f);
    }

    @Test
    public void testExpectUnsigned() throws IOException {
        writer.writeUInt8(1);
        writer.writeUInt16(2);
        writer.writeUInt24(3);
        writer.writeUInt32(4);
        writer.writeUInt32(-33);

        try {
            writer.writeUnsigned(8, 5);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 5", e.getMessage());
        }

        writer.writeUnsigned(5, 4);
        writer.writeUnsigned(6, 3);
        writer.writeUnsigned(7, 2);
        writer.writeUnsigned(8, 1);
        writer.writeUnsigned(-42, 4);

        try {
            writer.writeUnsigned(8, 8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        writer.flush();

        BinaryInputStream reader = getReader();

        try {
            reader.expectUnsigned(5);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 5", e.getMessage());
        }

        assertEquals(1, reader.expectUInt8());
        assertEquals(2, reader.expectUInt16());
        assertEquals(3, reader.expectUInt24());
        assertEquals(4, reader.expectUInt32());
        assertEquals(-33, reader.expectUInt32());

        try {
            reader.expectUnsigned(8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        assertEquals(5, reader.expectUnsigned(4));
        assertEquals(6, reader.expectUnsigned(3));
        assertEquals(7, reader.expectUnsigned(2));
        assertEquals(8, reader.expectUnsigned(1));
        assertEquals(-42, reader.expectUnsigned(4));

        assertEquals(reader.available(), 0);
    }

    @Test
    public void testExpectUnsignedLong() throws IOException {
        writer.writeUnsignedLong(1L, 1);
        writer.writeUnsignedLong(2L, 2);
        writer.writeUnsignedLong(3L, 3);
        writer.writeUnsignedLong(4L, 4);
        writer.writeUnsignedLong(0xF1234567L, 4);

        try {
            writer.writeUnsigned(8, 8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        writer.writeUnsignedLong(5L, 5);
        writer.writeUnsignedLong(6L, 6);
        writer.writeUnsignedLong(7L, 7);
        writer.writeUnsignedLong(8L, 8);

        try {
            writer.writeUnsignedLong(8, 9);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned long: 9", e.getMessage());
        }

        writer.writeULong32(0xF1234567L);
        writer.writeULong40(9L);
        writer.writeULong48(10L);
        writer.writeULong56(11L);
        writer.writeULong64(12L);

        BinaryInputStream reader = getReader();

        try {
            reader.expectUnsigned(8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        assertEquals(1L, reader.expectUnsignedLong(1));
        assertEquals(2L, reader.expectUnsignedLong(2));
        assertEquals(3L, reader.expectUnsignedLong(3));
        assertEquals(4L, reader.expectUnsignedLong(4));
        assertEquals(0xF1234567L, reader.expectUnsignedLong(4));

        try {
            reader.expectUnsignedLong(9);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned long: 9", e.getMessage());
        }

        assertEquals(5L, reader.expectUnsignedLong(5));
        assertEquals(6L, reader.expectUnsignedLong(6));
        assertEquals(7L, reader.expectUnsignedLong(7));
        assertEquals(8L, reader.expectUnsignedLong(8));

        assertEquals(0xF1234567L, reader.expectULong32());
        assertEquals(9L, reader.expectULong40());
        assertEquals(10L, reader.expectULong48());
        assertEquals(11L, reader.expectULong56());
        assertEquals(12L, reader.expectULong64());
    }

    @Test
    public void testReadUnsigned() throws IOException {
        writer.writeUInt16(2);

        BinaryInputStream reader = getReader();

        assertEquals(2, reader.readUInt16());
        assertEquals(0, reader.readUInt16());

        writer.writeUInt8(5);
        reader = getReader();

        try {
            reader.readUInt16();
            fail("No exception in bad read");
        } catch (IOException e) {
            assertEquals("Missing byte 2 to read uint16", e.getMessage());
        }
    }

    @Test
    public void testUnsigned() throws IOException {
        writer.writeUnsigned(1, 1);
        writer.writeUnsigned(2, 2);
        writer.writeUnsigned(3, 3);
        writer.writeUnsigned(4, 4);

        try {
            writer.writeUnsigned(8, 8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        BinaryInputStream reader = getReader();

        try {
            reader.expectUnsigned(8);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for unsigned: 8", e.getMessage());
        }

        assertEquals(1, reader.expectUnsigned(1));
        assertEquals(2, reader.expectUnsigned(2));
        assertEquals(3, reader.expectUnsigned(3));
        assertEquals(4, reader.expectUnsigned(4));
    }

    @Test
    public void testBadUnsigned() {
        assertBadExpectUnsigned("Missing unsigned byte", new byte[]{}, 1);

        assertBadExpectUnsigned("Missing byte 1 to expected uint16", new byte[]{}, 2);
        assertBadExpectUnsigned("Missing byte 2 to expected uint16", new byte[]{0}, 2);

        assertBadExpectUnsigned("Missing byte 1 to expected uint24", new byte[]{}, 3);
        assertBadExpectUnsigned("Missing byte 2 to expected uint24", new byte[]{0}, 3);
        assertBadExpectUnsigned("Missing byte 3 to expected uint24", new byte[]{0, 0}, 3);

        assertBadExpectUnsigned("Missing byte 1 to expected int", new byte[]{}, 4);
        assertBadExpectUnsigned("Missing byte 2 to expected int", new byte[]{0}, 4);
        assertBadExpectUnsigned("Missing byte 3 to expected int", new byte[]{0, 0}, 4);
        assertBadExpectUnsigned("Missing byte 4 to expected int", new byte[]{0, 0, 0}, 4);
    }

    private void assertBadExpectUnsigned(String message, byte[] data, int bytes) {
        ByteArrayInputStream bais = new ByteArrayInputStream(data);
        try {
            BinaryInputStream reader = makeInputStream(bais);
            reader.expectUnsigned(bytes);
            fail("No exception on bad short");
        } catch (IOException e) {
            assertEquals(message, e.getMessage());
        }
    }

    @Test
    public void testSigned() throws IOException {
        writer.writeSigned(-1, 1);
        writer.writeSigned(-2, 2);
        writer.writeSigned(-3, 4);
        writer.writeSigned(-4, 8);
        writer.writeSigned(-100L, 1);
        writer.writeSigned(-200L, 2);
        writer.writeSigned(-300L, 4);
        writer.writeSigned(-400L, 8);

        try {
            writer.writeSigned(-8, 3);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for signed: 3", e.getMessage());
        }

        try {
            writer.writeSigned(-8L, 3);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for signed: 3", e.getMessage());
        }

        BinaryInputStream reader = getReader();

        try {
            reader.expectSigned(3);
            fail("No exception on bad argument");
        } catch (IllegalArgumentException e) {
            assertEquals("Unsupported byte count for signed: 3", e.getMessage());
        }

        assertEquals(-1, reader.expectSigned(1));
        assertEquals(-2, reader.expectSigned(2));
        assertEquals(-3, reader.expectSigned(4));
        assertEquals(-4, reader.expectSigned(8));
    }

    @Test
    public void testZigzag() throws IOException {
        // test integer (32 bit) varints.
        testZigzag(0, 1);
        testZigzag(1, 1);
        testZigzag(-1, 1);
        testZigzag(0xcafe, 3);
        testZigzag(-123456, 3);
        testZigzag(615671317, 5);
        testZigzag(Integer.MIN_VALUE, 5);
        testZigzag(Integer.MAX_VALUE, 5);

        // test long (64 bit) varints.

        testZigzag(0L, 1);
        testZigzag(1L, 1);
        testZigzag(-1L, 1);
        testZigzag(0xcafeL, 3);
        testZigzag(-123456L, 3);

        testZigzag(1234567890123456789L, 9);
        testZigzag(0xcafebabedeadbeefL, 9);
    }

    private void testZigzag(int value, int bytes) throws IOException {
        out.reset();
        writer.writeZigzag(value);
        writer.writeZigzag(value);
        assertEquals(bytes * 2, out.size());
        var reader = getReader();
        assertEquals(value, reader.readIntZigzag());
        assertEquals(value, reader.expectIntZigzag());
    }

    private void testZigzag(long value, int bytes) throws IOException {
        out.reset();
        writer.writeZigzag(value);
        writer.writeZigzag(value);
        assertEquals(bytes * 2, out.size());
        var reader = getReader();
        assertEquals(value, reader.readLongZigzag());
        assertEquals(value, reader.expectLongZigzag());
    }

    @Test
    public void testBase128() throws IOException {
        // test integer (32 bit) varints.
        assertBase128(0, 1);
        assertBase128(1, 1);
        assertBase128(0x7f, 1);
        assertBase128(0x80, 2);
        assertBase128(-1, 5);
        assertBase128(0xcafe, 3);
        assertBase128(-123456, 5);
        assertBase128(Integer.MIN_VALUE, 5);
        assertBase128(Integer.MAX_VALUE, 5);

        // test long (64 bit) varints.

        assertBase128(0L, 1);
        assertBase128(1L, 1);
        assertBase128(0x7fL, 1);
        assertBase128(0x80L, 2);
        assertBase128(-1L, 10);
        assertBase128(0xcafeL, 3);
        assertBase128(-123456L, 10);
        assertBase128(Long.MIN_VALUE, 10);
        assertBase128(Long.MAX_VALUE, 9);

        assertBase128(1234567890123456789L, 9);
        assertBase128(0xcafebabedeadbeefL, 10);
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testEmptyBase128() throws IOException {
        assertEquals(0, getReader().readIntVarint());
        assertEquals(0L, getReader().readLongVarint());
        try {
            getReader().expectIntBase128();
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Missing unsigned byte"));
        }
        try {
            getReader().expectLongBase128();
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Missing unsigned byte"));
        }
    }

    private void assertBase128(int value, int bytes) throws IOException {
        out.reset();
        writer.writeBase128(value);
        assertEquals(bytes, out.size());
        BinaryInputStream reader = getReader();
        assertEquals(value, reader.readIntBase128());
    }

    private void assertBase128(long value, int bytes) throws IOException {
        out.reset();
        writer.writeBase128(value);
        assertEquals(bytes, out.size());
        BinaryInputStream reader = getReader();
        assertEquals(value, reader.readLongBase128());
    }

    @Test
    @SuppressWarnings("deprecation")
    public void testEmptyVarInt() throws IOException {
        assertEquals(0, getReader().readIntVarint());
        assertEquals(0L, getReader().readLongVarint());
    }

    @Test
    public void testVarInt() throws IOException {
        // test integer (32 bit) varints.
        testVarint(0, 1);
        testVarint(1, 1);
        testVarint(-1, 5);
        testVarint(0xcafe, 3);
        testVarint(-123456, 5);
        testVarint(Integer.MIN_VALUE, 5);
        testVarint(Integer.MAX_VALUE, 5);

        // test long (64 bit) varints.

        testVarint(0L, 1);
        testVarint(1L, 1);
        testVarint(-1L, 10);
        testVarint(0xcafeL, 3);
        testVarint(-123456L, 10);
        testVarint(Long.MIN_VALUE, 10);
        testVarint(Long.MAX_VALUE, 9);

        testVarint(1234567890123456789L, 9);
        testVarint(0xcafebabedeadbeefL, 10);
    }

    @SuppressWarnings("deprecation")
    private void testVarint(int value, int bytes) throws IOException {
        out.reset();
        writer.writeVarint(value);
        assertEquals(bytes, out.size());
        BinaryInputStream reader = getReader();
        assertEquals(value, reader.readIntBase128());
    }

    @SuppressWarnings("deprecation")
    private void testVarint(long value, int bytes) throws IOException {
        out.reset();
        writer.writeVarint(value);
        assertEquals(bytes, out.size());
        BinaryInputStream reader = getReader();
        assertEquals(value, reader.readLongBase128());
    }

    @Test
    public void testExpectLarge() throws IOException {
        byte[] arr = new byte[1024 * 1024]; // 1 MB.
        new Random().nextBytes(arr);
        var in = new ByteArrayInputStream(arr);
        var reader = makeInputStream(in);

        byte[] res = reader.expectBytes(arr.length);

        assertThat(arr, is(equalTo(res)));
    }

    @Test
    public void testExpectSmall() throws IOException {
        byte[] arr = new byte[1024]; // 1 kB.
        new Random().nextBytes(arr);
        var in = new ByteArrayInputStream(arr);
        var reader = makeInputStream(in);

        byte[] res = reader.expectBytes(arr.length);

        assertThat(arr, is(equalTo(res)));
    }

    @Test
    public void testExpectTooLarge() {
        byte[] arr = new byte[1024 * 10]; // 10 kB.
        new Random().nextBytes(arr);
        var in = new ByteArrayInputStream(arr);
        var reader = makeInputStream(in);

        try {
            reader.expectBytes(1024 * 1024 * 10);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Not enough data available on stream: 10240 < 10485760"));
        }
    }

    @Test
    public void testMarkReset() throws IOException {
        var a = new Random().nextLong();
        var b = new Random().nextLong();
        writer.writeLong(a);
        writer.writeLong(b);
        var reader = getReader();
        assertThat(reader.markSupported(), is(true));
        reader.mark(-1);
        assertThat(reader.expectLong(), is(a));
        reader.reset();
        assertThat(reader.expectLong(), is(a));
        assertThat(reader.expectLong(), is(b));
    }
}
