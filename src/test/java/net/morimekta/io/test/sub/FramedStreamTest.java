package net.morimekta.io.test.sub;

import net.morimekta.io.sub.FramedInputStream;
import net.morimekta.io.sub.FramedOutputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class FramedStreamTest {
    @Test
    public void testSimple() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(42);
        try (FramedOutputStream fos = new FramedOutputStream(baos)) {
            fos.write(42);
            fos.flush();
            fos.write(new byte[]{1, 21, 43});
        }
        baos.write(42);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

        assertThat(bais.read(), is(42));
        try (FramedInputStream fis = new FramedInputStream(bais)) {
            assertThat(fis.read(), is(42));
            assertThat(fis.getFrameSize(), is(4));
            assertThat(fis.available(), is(3));
            assertThat(fis.readAllBytes(), is(new byte[]{1, 21, 43}));
            assertThat(fis.available(), is(0));
        }
        assertThat(bais.read(), is(42));
        assertThat(bais.read(), is(-1));
    }

    @Test
    public void testBufferOverflow() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (FramedOutputStream fos = new FramedOutputStream(baos, 8)) {
            fos.write(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6});
            fos.write(7);
        }
        assertThat(baos.toByteArray(), is(new byte[]{
                17, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7
        }));
    }

    @Test
    public void testCloseBeforeRead() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[]{
                8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6
        });
        try (FramedInputStream bis = new FramedInputStream(in)) {
            assertThat(bis.read(), is(1));
            assertThat(bis.read(), is(2));
        }
        assertThat(in.read(), is(9));
        assertThat(in.read(), is(0));
    }

    @Test
    public void testMark() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[]{
                8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6
        });
        try (FramedInputStream fis = new FramedInputStream(in)) {
            assertThat(fis.markSupported(), is(true));
            assertThat(fis.read(), is(1));
            fis.mark(5);
            assertThat(fis.read(), is(2));
            assertThat(fis.read(), is(3));
            fis.reset();
            assertThat(fis.read(), is(2));
            assertThat(fis.read(), is(3));
        }
        assertThat(in.read(), is(9));
        assertThat(in.read(), is(0));
    }

    @Test
    public void testMark_notSupported() throws IOException {
        ByteArrayInputStream inNoMark = new ByteArrayInputStream(new byte[]{
                8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6
        }) {
            @Override
            public boolean markSupported() {
                return false;
            }

            @Override
            public void mark(int readAheadLimit) {
            }

            @Override
            public synchronized void reset() {
                // using UOE as ByteArrayInputStream#reset() does not declare IOException.
                throw new UnsupportedOperationException("mark/reset not supported");
            }
        };
        try (FramedInputStream fis = new FramedInputStream(inNoMark)) {
            assertThat(fis.markSupported(), is(false));
            assertThat(fis.read(), is(1));
            fis.mark(5);
            assertThat(fis.read(), is(2));
            fis.reset();
            fail("No exception");
        } catch (UnsupportedOperationException e) {
            assertThat(e.getMessage(), is("mark/reset not supported"));
        }
        assertThat(inNoMark.read(), is(9));
        assertThat(inNoMark.read(), is(0));

    }
}
