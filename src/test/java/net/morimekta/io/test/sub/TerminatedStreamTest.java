package net.morimekta.io.test.sub;

import net.morimekta.io.sub.TerminatedInputStream;
import net.morimekta.io.sub.TerminatedOutputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TerminatedStreamTest {
    @Test
    public void testSimple() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(42);
        try (TerminatedOutputStream ntos = new TerminatedOutputStream(baos)) {
            ntos.write(42);
            ntos.flush();
            ntos.write(new byte[]{1, 21, 43});
        }
        baos.write(42);

        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());

        assertThat(bais.read(), is(42));
        try (TerminatedInputStream ntis = new TerminatedInputStream(bais)) {
            assertThat(ntis.read(), is(42));
            assertThat(ntis.available(), is(1));
            assertThat(ntis.readAllBytes(), is(new byte[]{1, 21, 43}));
            assertThat(ntis.available(), is(0));
        }
        assertThat(bais.read(), is(42));
        assertThat(bais.read(), is(-1));
    }

    @Test
    public void testCloseBeforeComplete() throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(new byte[]{
                1, 2, 3, 4, 0, 5
        });
        try (TerminatedInputStream ntis = new TerminatedInputStream(in)) {
            assertThat(ntis.read(), is(1));
        }
        assertThat(in.read(), is(5));
        assertThat(in.read(), is(-1));
    }
}
