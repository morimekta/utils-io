/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.test.tty;

import net.morimekta.io.tty.TTY;
import net.morimekta.io.tty.TTYMode;
import net.morimekta.io.tty.TTYSize;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Clock;

import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Tests for the terminal size utility.
 */
public class TTYTest {
    private Runtime runtime;
    private TTY     tty;

    @BeforeEach
    public void setUp() {
        runtime = mock(Runtime.class);
        tty = new TTY(runtime, Clock.systemUTC());
    }

    @Test
    public void testConstructor() {
        TTY terminal = new TTY();
        // does not throw exception.
        terminal.clearCachedTerminalSize()
                .isInteractive();
    }

    @Test
    public void getTerminalSize() throws InterruptedException, IOException {
        Process process = mock(Process.class);

        when(runtime.exec(any(String[].class))).thenReturn(process);

        when(process.waitFor()).thenReturn(0);
        when(process.getErrorStream())
                .thenReturn(new ByteArrayInputStream(new byte[]{}));
        when(process.getInputStream())
                .thenReturn(new ByteArrayInputStream("65 129\n".getBytes(US_ASCII)));

        assertThat(tty.toString(),
                   is("STTY{mode=COOKED, size=tty(rows:65, cols:129)}"));
        assertThat(tty.isInteractive(), is(true));
        assertThat(tty.getTerminalSize(), CoreMatchers.is(new TTYSize(65, 129)));

        verify(runtime).exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"});
        verify(process).waitFor();
        verify(process).getErrorStream();
        verify(process).getInputStream();
        verifyNoMoreInteractions(runtime, process);
    }

    @Test
    public void getTerminalSize_NonInteractive() throws IOException, InterruptedException {
        Process process = mock(Process.class);
        when(runtime.exec(any(String[].class))).thenReturn(process);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(
                new ByteArrayInputStream("Unknown device /dev/tty\n".getBytes(US_ASCII)));

        assertThat(tty.isInteractive(), is(false));
        assertThat(tty.toString(), is("STTY{mode=COOKED}"));
        try {
            tty.getTerminalSize();
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getCause().getMessage(),
                       is("Unable to get TTY size: Unknown device /dev/tty"));
        }

        verify(runtime).exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"});
        verify(process).waitFor();
        verify(process).getErrorStream();
        verifyNoMoreInteractions(runtime, process);
    }

    @Test
    public void getTerminalSize_Interrupted() throws IOException, InterruptedException {
        Process process = mock(Process.class);

        when(runtime.exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"})).thenReturn(process);
        when(process.waitFor()).thenThrow(new InterruptedException("foo"));

        try {
            tty.getTerminalSize();
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(),
                       is("java.io.IOException: Unable to get TTY size: foo"));
        }
    }

    @Test
    public void getTerminalSize_EmptyOut() throws IOException, InterruptedException {
        Process process = mock(Process.class);

        when(runtime.exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"})).thenReturn(process);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("".getBytes(UTF_8)));
        when(process.getInputStream()).thenReturn(new ByteArrayInputStream("".getBytes(UTF_8)));

        try {
            tty.getTerminalSize();
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: No 'stty size' output."));
        }
    }

    @Test
    public void getTerminalSize_BadOut() throws IOException, InterruptedException {
        Process process = mock(Process.class);

        when(runtime.exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"})).thenReturn(process);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("".getBytes(UTF_8)));
        when(process.getInputStream()).thenReturn(new ByteArrayInputStream("foo\n".getBytes(UTF_8)));

        try {
            tty.getTerminalSize();
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: Unknown 'stty size' output: 'foo'"));
        }
    }

    @Test
    public void getTerminalSize_BadOut2() throws IOException, InterruptedException {
        Process process = mock(Process.class);

        when(runtime.exec(new String[]{"/bin/sh", "-c", "stty size </dev/tty"})).thenReturn(process);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("".getBytes(UTF_8)));
        when(process.getInputStream()).thenReturn(new ByteArrayInputStream("foo bar\n".getBytes(UTF_8)));

        try {
            tty.getTerminalSize();
            fail("no exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("java.io.IOException: Invalid stty size line 'foo bar'"));
        }
    }

    @Test
    public void testGetAndUpdateMode() throws IOException, InterruptedException {
        assertThat(tty.getCurrentMode(), CoreMatchers.is(TTYMode.COOKED));

        Process process1 = mock(Process.class, "P1");
        when(runtime.exec(any(String[].class))).thenReturn(process1);
        when(process1.waitFor()).thenReturn(0);
        when(process1.getErrorStream()).thenReturn(new ByteArrayInputStream(new byte[]{}));

        assertThat(tty.getAndUpdateMode(TTYMode.RAW), is(TTYMode.COOKED));
        assertThat(tty.getCurrentMode(), is(TTYMode.RAW));

        verify(runtime).exec(new String[]{"/bin/sh", "-c", "stty raw -echo </dev/tty"});
        verify(process1).waitFor();
        verify(process1).getErrorStream();
        verifyNoMoreInteractions(runtime, process1);
        reset(runtime);

        Process process2 = mock(Process.class, "P2");
        when(runtime.exec(any(String[].class))).thenReturn(process2);
        when(process2.waitFor()).thenReturn(0);
        when(process2.getErrorStream()).thenReturn(new ByteArrayInputStream(new byte[]{}));

        assertThat(tty.getAndUpdateMode(TTYMode.COOKED), is(TTYMode.RAW));
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));

        verify(runtime).exec(new String[]{"/bin/sh", "-c", "stty -raw echo </dev/tty"});
        verify(process2).waitFor();
        verify(process2).getErrorStream();
        verifyNoMoreInteractions(runtime, process1, process2);
    }

    @Test
    public void testGetAndUpdateMode_NoChange() throws IOException {
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));

        assertThat(tty.getAndUpdateMode(TTYMode.COOKED), is(TTYMode.COOKED));
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));

        verifyNoInteractions(runtime);
    }

    @Test
    public void testGetAndUpdateMode_Interrupted() throws IOException, InterruptedException {
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));

        Process process1 = mock(Process.class, "P1");
        when(runtime.exec(any(String[].class))).thenReturn(process1);
        when(process1.waitFor()).thenThrow(new InterruptedException("interrupted"));

        try {
            tty.getAndUpdateMode(TTYMode.RAW);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("interrupted"));
        }
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));
    }

    @Test
    public void testGetAndUpdateMode_Error() throws IOException, InterruptedException {
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));

        Process process1 = mock(Process.class, "P1");
        when(runtime.exec(any(String[].class))).thenReturn(process1);
        when(process1.waitFor()).thenReturn(1);
        when(process1.getErrorStream()).thenReturn(new ByteArrayInputStream("Unknown device /dev/tty".getBytes(UTF_8)));

        try {
            tty.getAndUpdateMode(TTYMode.RAW);
            fail("no exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Unknown device /dev/tty"));
        }
        assertThat(tty.getCurrentMode(), is(TTYMode.COOKED));
    }
}
