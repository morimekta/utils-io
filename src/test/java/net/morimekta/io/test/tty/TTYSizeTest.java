/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.test.tty;

import net.morimekta.io.tty.TTYSize;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;

public class TTYSizeTest {
    @Test
    public void testTerminalSize() {
        TTYSize term  = new TTYSize(12, 34);
        TTYSize same  = new TTYSize(12, 34);
        TTYSize other = new TTYSize(23, 45);

        assertThat(term.rows, is(12));

        assertThat(term, is(term));
        assertThat(term, is(same));
        assertThat(term, is(not(other)));
        assertThat(term.hashCode(), is(term.hashCode()));
        assertThat(term.hashCode(), is(not(new TTYSize(42, 42))));
        assertThat(term.equals(null), is(false));
        assertThat(term.equals(new Object()), is(false));
    }
}
