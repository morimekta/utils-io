/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.test.tty;

import net.morimekta.io.tty.TTY;
import net.morimekta.io.tty.TTYMode;
import net.morimekta.io.tty.TTYModeSwitcher;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * Testing the TTY Mode switcher.
 */
public class TTYModeSwitcherTest {
    @Test
    public void testSwitch() throws IOException {
        TTY terminal = mock(TTY.class);
        when(terminal.getAndUpdateMode(TTYMode.RAW)).thenReturn(TTYMode.COOKED);
        when(terminal.getAndUpdateMode(TTYMode.COOKED)).thenReturn(TTYMode.RAW);

        try (TTYModeSwitcher sms = new TTYModeSwitcher(terminal, TTYMode.RAW)) {
            verify(terminal).getAndUpdateMode(TTYMode.RAW);
            assertThat(sms.getBefore(), is(TTYMode.COOKED));
            assertThat(sms.getMode(), is(TTYMode.RAW));
            assertThat(sms.didChangeMode(), is(true));
            assertThat(sms.toString(), is("TerminalModeSwitcher{before=COOKED, mode=RAW}"));
        }
        verify(terminal).getAndUpdateMode(TTYMode.COOKED);
        verifyNoMoreInteractions(terminal);
    }

    @Test
    public void testSwitch_noChange() throws IOException {
        TTY terminal = mock(TTY.class);
        when(terminal.getAndUpdateMode(TTYMode.COOKED))
                .thenReturn(TTYMode.COOKED)
                .thenReturn(TTYMode.COOKED);

        try (TTYModeSwitcher sms = new TTYModeSwitcher(terminal, TTYMode.COOKED)) {
            assertThat(sms.getBefore(), is(TTYMode.COOKED));
            assertThat(sms.getMode(), is(TTYMode.COOKED));
            assertThat(sms.didChangeMode(), is(false));
            assertThat(sms.toString(), is("TerminalModeSwitcher{before=COOKED, mode=COOKED}"));
        }
        verify(terminal, times(2)).getAndUpdateMode(TTYMode.COOKED);
        verifyNoMoreInteractions(terminal);
    }
}
