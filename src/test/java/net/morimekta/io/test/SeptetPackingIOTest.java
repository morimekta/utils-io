package net.morimekta.io.test;

import net.morimekta.collect.util.Binary;
import net.morimekta.io.BitPackingInputStream;
import net.morimekta.io.BitPackingOutputStream;
import net.morimekta.io.SeptetPackingInputStream;
import net.morimekta.io.SeptetPackingOutputStream;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class SeptetPackingIOTest {
    @Test
    public void testStream() throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        SeptetPackingOutputStream spos = new SeptetPackingOutputStream(os);
        spos.write(1);
        spos.write(2);
        spos.write(3);
        spos.write(4);
        spos.write(5);
        spos.flush();
        spos.close();

        assertThat(Binary.wrap(os.toByteArray()).toBase64(), is("AggYQKA"));
        assertThat(Binary.wrap(os.toByteArray()).toHexString(), is("02081840a0"));

        ByteArrayInputStream in = new ByteArrayInputStream(os.toByteArray());
        SeptetPackingInputStream spis = new SeptetPackingInputStream(in);

        assertThat(spis.available(), is(5));
        assertThat(spis.read(), is(1));
        assertThat(spis.read(), is(2));
        assertThat(spis.read(), is(3));
        assertThat(spis.available(), is(2));
        assertThat(spis.read(), is(4));
        assertThat(spis.read(), is(5));
        assertThat(spis.read(), is(-1));
        assertThat(spis.available(), is(0));
        spis.close();
        assertThat(spis.available(), is(0));
    }

    @Test
    public void testClosed() throws IOException {
        SeptetPackingOutputStream os = new SeptetPackingOutputStream(new BitPackingOutputStream(new ByteArrayOutputStream()));
        os.close();
        try {
            os.write(55);
            fail("No exception");
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Writing to closed stream"));
        }

        SeptetPackingInputStream in = new SeptetPackingInputStream(new BitPackingInputStream(new ByteArrayInputStream(new byte[0])));
        in.close();
        try {
            fail("No exception: " + in.read());
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Reading from closed stream"));
        }
    }
}
