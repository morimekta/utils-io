/**
 * Java module with utilities for handling binary IO and controlling
 * IO mode on a *NIX terminal.
 */
module net.morimekta.io.test {
    exports net.morimekta.io.test to org.junit.platform.commons;
    exports net.morimekta.io.test.proc to org.junit.platform.commons;
    exports net.morimekta.io.test.tty to org.junit.platform.commons;
    exports net.morimekta.io.test.sub to org.junit.platform.commons;

    requires net.morimekta.io;

    requires net.morimekta.collect;
    requires org.mockito;
    requires org.mockito.junit.jupiter;
    requires org.hamcrest;
    requires transitive org.junit.jupiter.api;
}