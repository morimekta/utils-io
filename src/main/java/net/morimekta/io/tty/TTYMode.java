/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io.tty;

/**
 * Which IO mode the TTY is in.
 */
public enum TTYMode {
    /**
     * Raw mode only moves the cursor according to the print and control
     * characters given. Input characters are not echoed back to the terminal.
     */
    RAW,

    /**
     * Cooked mode wraps output lines and returns to start of line after
     * newline (essentially an LF automatically makes a CR). Input characters
     * are echoed back to the terminal.
     */
    COOKED
}
