package net.morimekta.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * An input stream that reads
 */
public class SeptetPackingInputStream extends InputStream {
    private BitPackingInputStream in;

    /**
     * Create a septet packing input stream.
     *
     * @param in Input stream to read packed bytes from.
     */
    public SeptetPackingInputStream(InputStream in) {
        this.in = in instanceof BitPackingInputStream
                  ? (BitPackingInputStream) in
                  : new BitPackingInputStream(in);
    }

    @Override
    public int read() throws IOException {
        if (in == null) {
            throw new IOException("Reading from closed stream");
        }
        return in.readBits(7);
    }

    @Override
    public int available() throws IOException {
        return in == null ? 0 : in.available(7);
    }

    @Override
    public void close() throws IOException {
        if (in != null) {
            try {
                in.close();
            } finally {
                in = null;
            }
        }
    }
}
