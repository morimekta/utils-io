/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * IO-Optimized binary writer. This is somewhat similar to the native
 * java {@link java.io.ObjectOutput}, but that it
 */
public abstract class BinaryOutputStream extends OutputStream {
    /**
     * Output stream.
     */
    protected final OutputStream out;

    /**
     * Constructor for binary output stream.
     *
     * @param out The output stream to write data to.
     */
    protected BinaryOutputStream(OutputStream out) {
        this.out = out;
    }

    @Override
    public void write(int b) throws IOException {
        out.write(b);
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        out.write(bytes);
    }

    @Override
    public void write(byte[] bytes, int off, int len) throws IOException {
        out.write(bytes, off, len);
    }

    @Override
    public void close() throws IOException {
        out.close();
    }

    @Override
    public void flush() throws IOException {
        out.flush();
    }

    /**
     * Write a signed byte to the output stream.
     *
     * @param integer The number to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeByte(byte integer) throws IOException {
        out.write(integer);
    }

    /**
     * Write a signed short to the output stream.
     *
     * @param integer The number to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeShort(short integer) throws IOException;

    /**
     * Write a signed int to the output stream.
     *
     * @param integer The number to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeInt(int integer) throws IOException;

    /**
     * Write a signed long to the output stream.
     *
     * @param integer The number to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeLong(long integer) throws IOException;

    /**
     * Write a float value to stream.
     *
     * @param value The double value to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeFloat(float value) throws IOException {
        writeInt(Float.floatToIntBits(value));
    }

    /**
     * Write a double value to stream.
     *
     * @param value The double value to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeDouble(double value) throws IOException {
        writeLong(Double.doubleToLongBits(value));
    }

    /**
     * @param number Unsigned :8 to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeUInt8(int number) throws IOException {
        out.write(number);
    }

    /**
     * @param number Unsigned :16 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeUInt16(int number) throws IOException;

    /**
     * @param number Unsigned :24 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeUInt24(int number) throws IOException;

    /**
     * @param number Unsigned :32 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeUInt32(int number) throws IOException;

    /**
     * @param number Unsigned :32 to write, but a long number.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeULong32(long number) throws IOException;

    /**
     * @param number Unsigned :40 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeULong40(long number) throws IOException;

    /**
     * @param number Unsigned :48 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeULong48(long number) throws IOException;

    /**
     * @param number Unsigned :56 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeULong56(long number) throws IOException;

    /**
     * @param number Unsigned :64 to write.
     * @throws IOException if unable to write to stream.
     */
    public abstract void writeULong64(long number) throws IOException;

    /**
     * @param number Unsigned integer to write.
     * @param bytes  Number of bytes to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeUnsigned(int number, int bytes) throws IOException {
        switch (bytes) {
            case 4:
                writeUInt32(number);
                return;
            case 3:
                writeUInt24(number);
                return;
            case 2:
                writeUInt16(number);
                return;
            case 1:
                writeUInt8(number);
                return;
        }
        throw new IllegalArgumentException("Unsupported byte count for unsigned: " + bytes);
    }

    /**
     * @param number Unsigned integer to write.
     * @param bytes  Number of bytes to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeUnsignedLong(long number, int bytes) throws IOException {
        switch (bytes) {
            case 8:
                writeULong64(number);
                return;
            case 7:
                writeULong56(number);
                return;
            case 6:
                writeULong48(number);
                return;
            case 5:
                writeULong40(number);
                return;
            case 4:
                writeUInt32((int) number);
                return;
            case 3:
                writeUInt24((int) number);
                return;
            case 2:
                writeUInt16((int) number);
                return;
            case 1:
                writeUInt8((int) number);
                return;
        }
        throw new IllegalArgumentException("Unsupported byte count for unsigned long: " + bytes);
    }

    /**
     * @param number Signed integer to write.
     * @param bytes  Number of bytes to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeSigned(int number, int bytes) throws IOException {
        switch (bytes) {
            case 8:
                writeLong(number);
                return;
            case 4:
                writeInt(number);
                return;
            case 2:
                writeShort((short) number);
                return;
            case 1:
                writeByte((byte) number);
                return;
        }
        throw new IllegalArgumentException("Unsupported byte count for signed: " + bytes);
    }

    /**
     * @param number Signed integer to write.
     * @param bytes  Number of bytes to write.
     * @throws IOException if unable to write to stream.
     */
    public void writeSigned(long number, int bytes) throws IOException {
        switch (bytes) {
            case 8:
                writeLong(number);
                return;
            case 4:
                writeInt((int) number);
                return;
            case 2:
                writeShort((short) number);
                return;
            case 1:
                writeByte((byte) number);
                return;
        }
        throw new IllegalArgumentException("Unsupported byte count for signed: " + bytes);
    }

    /**
     * Write a long number as zigzag encoded to the stream. The least
     * significant bit becomes the sign, and the actual value is mad absolute
     * and shifted one bit. This makes it maximum compressed both when positive
     * and negative.
     *
     * @param number The number to write.
     * @return Number of bytes written.
     * @throws IOException if unable to write to stream.
     */
    public int writeZigzag(int number) throws IOException {
        return writeBase128((number << 1) ^ (number >> 31));
    }

    /**
     * Write a long number as zigzag encoded to the stream. The least
     * significant bit becomes the sign, and the actual value is mad absolute
     * and shifted one bit. This makes it maximum compressed both when positive
     * and negative.
     *
     * @param number The number to write.
     * @return Number of bytes written.
     * @throws IOException if unable to write to stream.
     */
    public int writeZigzag(long number) throws IOException {
        return writeBase128((number << 1) ^ (number >> 63));
    }

    /**
     * Write a signed number as varint (integer with variable number of bytes,
     * determined as part of the bytes themselves.
     *
     * @param number The number to write.
     * @return Number of bytes written.
     * @throws IOException if unable to write to stream.
     * @deprecated
     */
    @Deprecated
    public int writeVarint(int number) throws IOException {
        return writeBase128(number);
    }

    /**
     * Write a signed number as varint (integer with variable number of bytes,
     * determined as part of the bytes themselves.
     *
     * @param i The number to write.
     * @return The number of bytes written.
     * @throws IOException if unable to write to stream.
     */
    public abstract int writeBase128(int i) throws IOException;

    /**
     * Write a signed number as base 128 (integer with variable number of bytes,
     * determined as part of the bytes themselves.
     *
     * @param number The number to write.
     * @return The number of bytes written.
     * @throws IOException if unable to write to stream.
     * @deprecated
     */
    @Deprecated
    public int writeVarint(long number) throws IOException {
        return writeBase128(number);
    }

    /**
     * Write a signed number as base 128 (integer with variable number of bytes,
     * determined as part of the bytes themselves.
     *
     * @param number The number to write.
     * @return The number of bytes written.
     * @throws IOException if unable to write to stream.
     */
    public abstract int writeBase128(long number) throws IOException;
}
