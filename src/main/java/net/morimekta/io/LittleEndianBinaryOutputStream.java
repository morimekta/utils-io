/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * IO-Optimized binary writer using little-endian integer encoding.
 */
public class LittleEndianBinaryOutputStream extends BinaryOutputStream {
    /**
     * Create a little endian binary output stream.
     *
     * @param out Output stream to write data to.
     */
    public LittleEndianBinaryOutputStream(OutputStream out) {
        super(out);
    }

    @Override
    public void writeShort(short integer) throws IOException {
        out.write(integer);
        out.write(integer >>> 8);
    }

    @Override
    public void writeInt(int integer) throws IOException {
        out.write(integer);
        out.write(integer >>> 8);
        out.write(integer >>> 16);
        out.write(integer >>> 24);
    }

    @Override
    public void writeLong(long integer) throws IOException {
        out.write((int) (integer));
        out.write((int) (integer >>> 8));
        out.write((int) (integer >>> 16));
        out.write((int) (integer >>> 24));
        out.write((int) (integer >>> 32));
        out.write((int) (integer >>> 40));
        out.write((int) (integer >>> 48));
        out.write((int) (integer >>> 56));
    }

    @Override
    public void writeUInt16(int number) throws IOException {
        out.write(number);
        out.write(number >>> 8);
    }

    @Override
    public void writeUInt24(int number) throws IOException {
        out.write(number);
        out.write(number >>> 8);
        out.write(number >>> 16);
    }

    @Override
    public void writeUInt32(int number) throws IOException {
        out.write(number);
        out.write(number >>> 8);
        out.write(number >>> 16);
        out.write(number >>> 24);
    }

    @Override
    public void writeULong32(long number) throws IOException {
        out.write((int) (number));
        out.write((int) (number >>> 8));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 24));
    }

    @Override
    public void writeULong40(long number) throws IOException {
        out.write((int) (number));
        out.write((int) (number >>> 8));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 32));
    }

    @Override
    public void writeULong48(long number) throws IOException {
        out.write((int) (number));
        out.write((int) (number >>> 8));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 40));
    }

    @Override
    public void writeULong56(long number) throws IOException {
        out.write((int) (number));
        out.write((int) (number >>> 8));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 40));
        out.write((int) (number >>> 48));
    }

    @Override
    public void writeULong64(long number) throws IOException {
        out.write((int) (number));
        out.write((int) (number >>> 8));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 40));
        out.write((int) (number >>> 48));
        out.write((int) (number >>> 56));
    }

    @Override
    public int writeBase128(int i) throws IOException {
        int b = 1;
        boolean c = (i ^ (i & 0x7f)) != 0;
        out.write((c ? 0x80 : 0x00) | (i & 0x7f));
        while (c) {
            ++b;
            i >>>= 7;
            c = (i ^ (i & 0x7f)) != 0;
            out.write((c ? 0x80 : 0x00) | (i & 0x7f));
        }
        return b;
    }

    @Override
    public int writeBase128(long i) throws IOException {
        int b = 1;
        boolean c = (i ^ (i & 0x7f)) != 0;
        out.write((c ? 0x80 : 0x00) | (int) (i & 0x7f));
        while (c) {
            ++b;
            i >>>= 7;
            c = (i ^ (i & 0x7f)) != 0;
            out.write((c ? 0x80 : 0x00) | (int) (i & 0x7f));
        }
        return b;
    }
}
