package net.morimekta.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * An output stream that assumes each written byte is a septet (not an octet),
 * and packs each septet onto each other to make it more compact.
 */
public class SeptetPackingOutputStream extends OutputStream {
    private BitPackingOutputStream out;

    /**
     * Create a septet packing output stream.
     *
     * @param out Output stream to write packed bytes to.
     */
    public SeptetPackingOutputStream(OutputStream out) {
        this.out = out instanceof BitPackingOutputStream
                   ? (BitPackingOutputStream) out
                   : new BitPackingOutputStream(out);
    }

    @Override
    public void write(int septet) throws IOException {
        if (out == null) {
            throw new IOException("Writing to closed stream");
        }
        out.writeBits(7, septet % 0x7F);
    }

    @Override
    public void close() throws IOException {
        if (out != null) {
            try {
                out.close();
            } finally {
                out = null;
            }
        }
    }

    @Override
    public void flush() throws IOException {
        if (out != null) {
            out.flush();
        }
    }
}
