/*
 * Copyright (c) 2016, Stein Eldar Johnsen
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * IO-Optimized binary writer using big-endian integer encoding. See
 * {@link BinaryOutputStream} for details.
 */
public class BigEndianBinaryOutputStream extends BinaryOutputStream {
    /**
     * Create a big endian binary output stream.
     *
     * @param out Output stream to write data to.
     */
    public BigEndianBinaryOutputStream(OutputStream out) {
        super(out);
    }

    @Override
    public void writeShort(short integer) throws IOException {
        out.write(integer >>> 8);
        out.write(integer);
    }

    @Override
    public void writeInt(int integer) throws IOException {
        out.write(integer >>> 24);
        out.write(integer >>> 16);
        out.write(integer >>> 8);
        out.write(integer);
    }

    @Override
    public void writeLong(long integer) throws IOException {
        out.write((int) (integer >>> 56));
        out.write((int) (integer >>> 48));
        out.write((int) (integer >>> 40));
        out.write((int) (integer >>> 32));
        out.write((int) (integer >>> 24));
        out.write((int) (integer >>> 16));
        out.write((int) (integer >>> 8));
        out.write((int) (integer));
    }

    @Override
    public void writeUInt16(int number) throws IOException {
        out.write(number >>> 8);
        out.write(number);
    }

    @Override
    public void writeUInt24(int number) throws IOException {
        out.write(number >>> 16);
        out.write(number >>> 8);
        out.write(number);
    }

    @Override
    public void writeUInt32(int number) throws IOException {
        out.write(number >>> 24);
        out.write(number >>> 16);
        out.write(number >>> 8);
        out.write(number);
    }

    @Override
    public void writeULong32(long number) throws IOException {
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 8));
        out.write((int) (number));
    }

    @Override
    public void writeULong40(long number) throws IOException {
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 8));
        out.write((int) (number));
    }

    @Override
    public void writeULong48(long number) throws IOException {
        out.write((int) (number >>> 40));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 8));
        out.write((int) (number));
    }

    @Override
    public void writeULong56(long number) throws IOException {
        out.write((int) (number >>> 48));
        out.write((int) (number >>> 40));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 8));
        out.write((int) (number));
    }

    @Override
    public void writeULong64(long number) throws IOException {
        out.write((int) (number >>> 56));
        out.write((int) (number >>> 48));
        out.write((int) (number >>> 40));
        out.write((int) (number >>> 32));
        out.write((int) (number >>> 24));
        out.write((int) (number >>> 16));
        out.write((int) (number >>> 8));
        out.write((int) (number));
    }

    @Override
    public int writeBase128(int i) throws IOException {
        int blocks = (31 - Integer.numberOfLeadingZeros(i)) / 7;
        int n = 1;
        while (blocks > 0) {
            out.write(0x80 | (0x7f & ((i >> 7 * blocks))));
            ++n;
            --blocks;
        }
        out.write(0x7f & i);
        return n;
    }

    @Override
    public int writeBase128(long i) throws IOException {
        int blocks = (63 - Long.numberOfLeadingZeros(i)) / 7;
        int n = 1;
        while (blocks > 0) {
            out.write(0x80 | (0x7f & (int) ((i >> 7 * blocks))));
            ++n;
            --blocks;
        }
        out.write(0x7f & (int) i);
        return n;
    }
}
