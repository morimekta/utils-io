/**
 * This module contains stream classes that <b>partially</b>
 * wrap another stream. These will then make a stream for a
 * part of (sub) the original stream, and when closed will
 * let the enclosed stream continue as if a single value was
 * read.
 * <p>
 * Each stream is designed to handle a single enclosed sub-
 * part, and when closed will not read another part or frame.
 * <p>
 * Behavior is also always undefined if the streams are not
 * closed.
 */
package net.morimekta.io.sub;