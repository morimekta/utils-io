/**
 * Java module with utilities for handling binary IO and controlling
 * IO mode on a *NIX terminal.
 */
module net.morimekta.io {
    exports net.morimekta.io;
    exports net.morimekta.io.proc;
    exports net.morimekta.io.sub;
    exports net.morimekta.io.tty;
}